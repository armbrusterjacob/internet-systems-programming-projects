var str = "";
var style = "";
var dft;
var correct;
var pronoun;
var wrong;
function show(){
	var stuff = "<html lang='en-US'>" + style + "<body> " + str + "<script src='functions.js'></script>"
    var myWindow = window.open("", "MsgWindow", "width=800,height=600");
    myWindow.document.write(stuff);
}
function add(){
	str += "<h1>Welcome to my spnaish site!</h1><p>Enjoy the spanish content.</p>"
}

function toolTip(){
	str += "<p title='Translation: You generated this setence from the main page!'>Hover over to transle this: ¡Generaste este oración de la página principa!</p>"
}

function detectElement(){
	var det = "<h3>Accent Detection</h3>\
	<input id='dInput' type='text' ></input>\
	<button id='dButton' onclick='parent.detect();'>Verify</button>\
	<br>\
	<p id='dResult'>Enter a spnaish word  with an accent</p>\
	";
	str += det;
}
function indirect(){
	str += "<h2>Hover over the sentence to see what the pronoun refers to!</h2>\
	<p>The pronoun will be <span style='color:" + pronoun + "'> this color</span> and what its refering to will be <span style='color:" + correct + "'> this color.<p\>\
	<span onmouseover='this.style.color=\"" + wrong + "\"' onmouseout='this.style.color=\"" + dft + "\"'>Silva </span>\
	<span onmouseover='this.style.color=\"" + pronoun + "\"' onmouseout='this.style.color=\"" + dft + "\"'>les</span> \
	<span onmouseover='this.style.color=\"" + wrong + "\"' onmouseout='this.style.color=\"" + dft + "\"'>da </span>\
	<span onmouseover='this.style.color=\"" + wrong + "\"' onmouseout='this.style.color=\"" + dft + "\"'>los</span> \
	<span onmouseover='this.style.color=\"" + wrong + "\"' onmouseout='this.style.color=\"" + dft + "\"'>regalos</span> \
	<span onmouseover='this.style.color=\"" + correct + "\"' onmouseout='this.style.color=\"" + dft + "\"'>a nosotros</span>\
	<br\>";
}
function addAccent(){
	str += "<input type'text' id='accentForm'></input><br />\
	<button onclick=\"accent('\u00C1')\">\u00C1</button>\
	<button onclick=\"accent('\u00C9')\">\u00C9</button>\
	<button onclick=\"accent('\u00CD')\">\u00CD</button>\
	<button onclick=\"accent('\u00D1')\">\u00D1</button>\
	<button onclick=\"accent('\u00D3')\">\u00D3</button>\
	<button onclick=\"accent('\u00DA')\">\u00DA</button>\
	<button onclick=\"accent('\u00DC')\">\u00DC</button>\
	<br />\
	<button onclick=\"accent('\u00E1')\">\u00E1</button>\
	<button onclick=\"accent('\u00E9')\">\u00E9</button>\
	<button onclick=\"accent('\u00ED')\">\u00ED</button>\
	<button onclick=\"accent('\u00F1')\">\u00F1</button>\
	<button onclick=\"accent('\u00F3')\">\u00F3</button>\
	<button onclick=\"accent('\u00FA')\">\u00FA</button>\
	<button onclick=\"accent('\u00FC')\">\u00FC</button>\
	<br />";
}

function changeStyle(){
	if(document.getElementById('b').checked){
		dft = '#FFF9F3';
		correct = '#00CC66';
		pronoun = '#0066CC';
		wrong = '#CC0066';
		style = "<head><link rel='stylesheet' href='b.css'>\</head>";
	}else if(document.getElementById('c').checked){
		dft = '#000000';
		correct = '#00FF00';
		pronoun = '#0000FF';
		wrong = '#FF0000';
		style = "<head><link rel='stylesheet' href='c.css'>\</head>";
	}else{
		dft = '#000000';
		correct = '#00FF00';
		pronoun = '#CCCCFF';
		wrong = '#FF0000';
		style = "<head><link rel='stylesheet' href='a.css'>\</head>";
	}

}

function addDef(){
	str+= "<a href='definitions.xml'>Definitions</a><br />";
}

function addMedia(){
	str += '<iframe width="400" height="225" src="https://www.youtube.com/embed/i3rBUxcJQu8" frameborder="0" allowfullscreen></iframe><br />'
}

function addAlert(){
	str += '<img src="alert.png" alt="Click me!" onclick="alertMe()" style="width:180px;height:200px"><br />';
}