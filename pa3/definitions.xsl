<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>
<xsl:template match="/">
  <h2>Spanish Definitions</h2>
  <table style="width:300px">
	<tr>
	  <th style="text-align:left">Spanish</th>
	  <th style="text-align:left">English</th>
	</tr>
	<xsl:for-each select="dictionary/word">
	<tr>
	  <td><xsl:value-of select="spanish" /></td>
	  <td><xsl:value-of select="english" /></td>
	</tr>
	</xsl:for-each>
  </table>
</xsl:template>

</xsl:stylesheet>