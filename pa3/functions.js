function detect(){
	var word = document.getElementById('dInput').value;
	var verify = word.search(
	/\w*[\u00C1\u00C9\u00CD\u00D1\u00D3\u00DA\u00DC\u00E1\u00E9\u00ED\u00F1\u00F3\u00FA\u00FC]+\w*/);
	//unicode spanish characters
	if(verify != -1){
		document.getElementById('dResult').innerHTML = "This word has accents!";
		document.getElementById('dResult').style = "color:green";
	}else{
		document.getElementById('dResult').innerHTML = "This word doesn't have accents...";
		document.getElementById('dResult').style = "color:red";
	}
}

function accent(chr){
	document.getElementById('accentForm').value += chr;
}

function alertMe(){
	alert("\u00A1Hola! Soy una altera y diceme.");
}