USE test;
DROP TABLE IF EXISTS Users;

CREATE TABLE Users(
	Id INT(12) NOT NULL AUTO_INCREMENT,
	Username VARCHAR(40),
	Hash VARCHAR(64),
	PRIMARY KEY (Id)
);
INSERT INTO Users VALUES
(NULL, 'Jacob',  'abcusers'),
(NULL, 'sdf', 'sfads');